import { executeQuery } from "./db/executeQuery"

export interface IMessage {
    sender_key: string,
    message: string
}

export interface IAlterMessage {
    sender_key: string,
    message?: string,
    message_id: number
}

export const validateUser = async (userkey: string) => {
    try {
        const query = `SELECT user_key FROM Users WHERE user_key = $1`
        const result = await executeQuery(query, [userkey])
        if(result.rows.length === 1) {
            return result.rows[0]
        }
        return false
    } catch(e){
        return e
    }
}

export const getAllMessages = async () => {
    try {
        const query = `SELECT message_id, sender_key, sender_name, content, timestamp FROM Messages ORDER BY timestamp ASC`
        const result = await executeQuery(query)
        return result.rows
    } catch(e){
        return e
    }
}

export const getSingleMessage = async (message_id: number) => {
    try {
        const query = `SELECT message_id, sender_name, content, timestamp FROM Messages WHERE message_id = $1`
        const result = await executeQuery(query, [String(message_id)])
        return result.rows
    } catch(e){
        return e
    }
}

export const postNewMessage = async (message: IMessage) => {
    try {
        const params = [ message.sender_key, message.message ]
        console.log("params: ",params)
        const query = `
            INSERT INTO Messages 
                (sender_name, sender_key, content) 
            VALUES 
                ( 
                    (SELECT user_name FROM Users
                        WHERE user_key = $1),
                    $1, $2
                )
                RETURNING *`
        const result = await executeQuery(query, params)
        return result.rows[0]
    } catch(e){
        console.log("Error: ",e)
        return e
    }
}

export const deleteMessage = async (target: IAlterMessage) => {
    try {
        const query = `DELETE FROM Messages WHERE sender_key = $1 and message_id = $2 RETURNING message_id`
        const result = await executeQuery(query, [target.sender_key, String(target.message_id)])
        return result.rows[0]
    } catch(e){
        return e
    }
}

export const modifyMessage = async (message: IAlterMessage) => {
    try {
        const query = `
            UPDATE Messages SET
                content = $2
            WHERE sender_key = $1
            AND message_id = $3
            RETURNING *
        `
        const result = await executeQuery(query, [message.sender_key, String(message.message), String(message.message_id)])
        console.log("Result: ",result.rows[0])
        return result.rows[0]
    } catch(e){
        return e
    }
}
