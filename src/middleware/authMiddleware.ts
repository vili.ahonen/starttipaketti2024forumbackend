import { Request, Response, NextFunction } from "express";
import { validateUser } from "../messageController";

export interface IAuthenticatedUserRequest extends Request {
    userKey?: string
}
export const UserAuthentication = async (req: IAuthenticatedUserRequest, res: Response, next: NextFunction) => {
    const userKey = req.get("userkey")
    if (!userKey) {
        return res.status(401).send("Missing 'userkey' header.")
    } else{
        const validatedUserKey = await validateUser(String(userKey))
        console.log("Validation. ", validatedUserKey)
        if (validatedUserKey) {
            req.userKey = validatedUserKey
            console.log("req: ",req.userKey)
            next()
        } else {
            return res.status(401).send("Invalid 'userkey' header.")
        }
    }
}

export const AdminAuthentication = async (req: IAuthenticatedUserRequest, res: Response, next: NextFunction) => {
    const userKey = req.get("userkey")
    if (!userKey) {
        return res.status(401).send("Missing 'userkey' header.")
    } else{
        const adminKey = process.env.ADMIN
        console.log("adminKey. ", adminKey)
        if (adminKey === userKey) {
            next()
        } else {
            return res.status(401).send("Invalid 'userkey' header.")
        }
    }
}

