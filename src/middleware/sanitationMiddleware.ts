import { NextFunction, Request, Response } from "express";

export const ValidateNewMessage = (req: Request, res: Response, next: NextFunction) => {
    const body = req.body
    console.log("MEssage: ", body.message)
    if (!body.message) {
        return res.status(400).send("Missing 'message' property within body or it's empty")
    }
    if (body.message?.length > 500) {
        return res.status(400).send(`Message is too long. Maximum character count is 500 and you had ${body.message?.length}`)
    }
    next()
}

export const ValidateModifyMessage = (req: Request, res: Response, next: NextFunction) => {
    const body = req.body
    if (!body.id) {
        return res.status(400).send("Missing 'id' property within body")
    }
    console.log(typeof (body.id))
    if (typeof (body.id) !== "number") {
        return res.status(400).send("Invalid 'id' property, it should be number")
    }
    if (!body.message) {
        return res.status(400).send("Missing 'message' property within body or it's empty")
    }
    if (body.message?.length > 500) {
        return res.status(400).send(`Message is too long. Maximum character count is 500 and you had ${body.message?.length}`)
    }
    next()
}

export const fetchQueryValidation = (req: Request, res: Response, next: NextFunction) => {
    const order = req.query.order ? String(req.query.order).toLowerCase() : "desc"
    const orders = ["asc", "desc"]
    if (order && !orders.includes(order)) {
        return res.status(400).send("Query parameter 'order' must be either 'asc' or 'desc'.")
    }
    next()
}

export const ValidateId = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id
    if (!Number(id)) {
        return res.status(400).send("Invalid id. Id must be a number")
    }
    next()
}
