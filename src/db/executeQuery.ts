import pg from 'pg'
import "dotenv/config"
import { setTimeout } from "timers/promises";

const {PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE} = process.env;

const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: String(PG_PASSWORD),
    database: PG_DATABASE,
    ssl: true
});

export const executeQuery = async (query: string, params: string[] = []) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, params)
        return result
    } catch (error: any) {
        console.error(error.stack)
        error.name = 'dbError'
        throw error
    } finally {
        client.release()
    }
}

export const InitializeDatabase = async () => {
    const creationQuery = `
        DROP TABLE IF EXISTS Users CASCADE;
        DROP TABLE IF EXISTS Messages CASCADE;

        CREATE TABLE IF NOT EXISTS Users (
            user_id serial PRIMARY KEY,
            user_key VARCHAR (50) UNIQUE NOT NULL,
            user_name VARCHAR (50) UNIQUE
        );

        CREATE TABLE IF NOT EXISTS Messages (
            message_id serial PRIMARY KEY,
            sender_key VARCHAR (50) REFERENCES Users (user_key),
            sender_name VARCHAR (50) REFERENCES Users (user_name),
            content VARCHAR (500),
            timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
        );
    `
   
    console.log("INITIALIZE DB TABLES")
    try{
        await executeQuery(creationQuery)
        console.log("POPULATE TABLES: Users")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Frodo',        '7QUY8ujh');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Sam',          '6M3BAimf');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Pippin',       'rXAB22XP');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Merry',        'E9oJ3rf9');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Gandalf',      'D52vCZu4');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Aragorn',      'KlTCwivO');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Legolas',      'ZAlOk3LC');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Gimli',        'hdh2gMve');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Boromir',      'Oy1wbNqt');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Sauron',       'qQLmfgiV');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Galadriel',    'AJOAZ0qW');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Saruman',      'BE5AFqsf');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Faramir',      'bP2H3Uxc');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Gothmog',      '53IhAIdO');")
        await executeQuery("INSERT INTO Users (user_name, user_key) VALUES ('Gollum',       'C9CkOQ6d');")
        // Add admin too
        console.log("POPULATE TABLES: Messages")
        await executeQuery("INSERT INTO Messages (sender_name, sender_key, content) VALUES ( (SELECT user_name FROM Users WHERE user_id = 1), (SELECT user_key FROM Users WHERE user_id = 1), 'I''ll take the Ring to the Mordor');")
        await executeQuery("INSERT INTO Messages (sender_name, sender_key, content) VALUES ( (SELECT user_name FROM Users WHERE user_id = 9), (SELECT user_key FROM Users WHERE user_id = 9), 'One does not simply walk into Mordor');")
        await executeQuery("INSERT INTO Messages (sender_name, sender_key, content) VALUES ( (SELECT user_name FROM Users WHERE user_id = 2), (SELECT user_key FROM Users WHERE user_id = 2), 'Po-ta-tos!');")
        await executeQuery("INSERT INTO Messages (sender_name, sender_key, content) VALUES ( (SELECT user_name FROM Users WHERE user_id = 7), (SELECT user_key FROM Users WHERE user_id = 7), 'They are taking the hobbits to Isengard!');")
        console.log("INITIALIZATION COMPLETED")
    } catch (error){
        console.log("DB INIT ERROR\n",error)
    }
}