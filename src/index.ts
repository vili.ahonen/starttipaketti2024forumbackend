import express, { Request, Response } from "express"
import cors from "cors"
import { IAlterMessage, IMessage, deleteMessage, getAllMessages, getSingleMessage, modifyMessage, postNewMessage } from "./messageController"
import { InitializeDatabase } from "./db/executeQuery"
import { AdminAuthentication, IAuthenticatedUserRequest, UserAuthentication } from "./middleware/authMiddleware"
import { ValidateId, ValidateModifyMessage, ValidateNewMessage, fetchQueryValidation } from "./middleware/sanitationMiddleware"
import { NothingFound } from "./middleware/nothingFoundMiddleware"

const app = express()
app.use(express.json())
app.use(cors())
module.exports = app
const PORT = process.env.PORT

//InitializeDatabase()

app.get("/allMessages", fetchQueryValidation, async (req: Request, res: Response) => {
    const messages = await getAllMessages()
    res.status(200).send(messages)
})
app.get("/singleMessage/:id", async (req, res) => {
    const message_id = req.params.id
    const messages = await getSingleMessage(Number(message_id))
    res.send(messages)
})

app.post("/newMessage", UserAuthentication, ValidateNewMessage, async (req: IAuthenticatedUserRequest, res: Response) => {
    const { message } = req.body
    let key = req.userKey ? req.userKey : ""
    key = Object.values(key)[0]

    const body: IMessage = { sender_key: key, message: message}
    const result = await postNewMessage(body)
    const formattedResponse = {
        "id": Object.values(result)[0],
        "sender_key": Object.values(result)[1],
        "sender_name": Object.values(result)[2],
        "message": Object.values(result)[3],
        "timestamp" : Object.values(result)[4],
    }  
    res.status(201).send(formattedResponse)
})

app.delete("/deleteMessage/:id", UserAuthentication, ValidateId, async (req: IAuthenticatedUserRequest, res: Response) => {
    const id = req.params.id
    let key = req.userKey ? req.userKey : ""
    key = Object.values(key)[0]
    const target: IAlterMessage = { sender_key: key, message_id: Number(id)}
    const result = await deleteMessage(target)
    if (result) {
        return res.status(200).send(`Deleted message with id: ${Object.values(result)[0]}`)
    }
    res.status(200).send(`Message with id of ${id} was not found or it may not belong to you`)
})

app.put("/modifyMessage", UserAuthentication, ValidateModifyMessage, async (req: IAuthenticatedUserRequest, res: Response) => {
    const { id, message } = req.body
    let key = req.userKey ? req.userKey : ""
    key = Object.values(key)[0]

    const body: IAlterMessage = { sender_key: key, message: message, message_id: id }
    const result = await modifyMessage(body)
    if (result === undefined) {
        return res.status(401).send(`No message with id ${id} found or it doesn't belong to you`)
    }
     
    if (Object.values(result)[0] === id) {
        const formattedResponse = {
            "id": Object.values(result)[0],
            "sender_key": Object.values(result)[1],
            "sender_name": Object.values(result)[2],
            "message": Object.values(result)[3],
            "timestamp" : Object.values(result)[4],
        }  
        return res.status(201).send(formattedResponse)
    }    
})

// hidden and loosely protected database reset
app.post("/thyshallperish", AdminAuthentication, (req: IAuthenticatedUserRequest, res: Response) => {
    InitializeDatabase()
    res.status(200).send("Tom Bombadil did his magic!")
})

app.use(NothingFound)

app.listen(PORT, () => {
    console.log(`Listeing to port ${PORT}`)
})

export default app