# Starttipaketti2024ForumBackend


## Requirements
- Dockerized PostgreSQL database 

I used this following docker command to run up the database container:

`docker run --name forumDB --env POSTGRES_PASSWORD=forumpass --env POSTGRES_USER=forumuser -p 5432:5432 -d postgres:15.2
`

Open the container and create a database.

`docker exec -it forumDB psql -U forumuser`

In the PSQL Shell:

`CREATE DATABASE <dabatasename>`

All should be now ready.

## Getting started

Navigate to root. Add an `.env` file with following content:

```
PORT=<nodeport>
PG_HOST=<postgreSQL host>
PG_PORT=<postgreSQL port>
PG_USERNAME=<postgreSQL user>
PG_PASSWORD=<postgreSQL password>
PG_DATABASE=<postgreSQL database>
```

Before running, install all dependencies:

`npm i`

After successfull installation you can run the project by: `npm run dev`

## Initial Database Script
Initial script for database is by default commented out. To initialize database, first make sure you have database ready - local or containerized. Uncomment the 
`//InitializeDatabase()` row and run `npm run dev` once. If greets you with `INITIALIZATION COMPLETED` message, database is ready and the row can be commented. 

By default the scripts creates bunch of users with name and key, and few example messages with content, user key, sender name and timestamp. 
### NOTE: There is no user controller, no user can be added, modified or deleted yet

## API Documentation

<table >
                <tr>
                    <th>Method</th>
                    <th>Endpoint</th>
                    <th>Headers</th>
                    <th>Body</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>GET</td>
                    <td>/allMessages</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Returns all messages.</td>
                </tr>
                <tr>
                    <td>GET</td>
                    <td>/singleMessage/:id</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Returns single message by id.</td>
                </tr>
                <tr>
                    <td>POST</td>
                    <td>/newMessage</td>
                    <td>userKey</td>
                    <td>
                        {
                        "message": <message>
                        }
                    </td>
                    <td>Posts a new message that can be read.</td>
                </tr>
                <tr>
                    <td>DELETE</td>
                    <td>/deleteMessage/:id</td>
                    <td>userKey</td>
                    <td>-</td>
                    <td>Deletes a post by this user. Uses userKey to identify rightful owner.</td>
                </tr>
                <tr>
                    <td>PUT</td>
                    <td>/modifyMessage</td>
                    <td>userKey</td>
                    <td>
                        {
                        "id": <post_id>,
                        "message": <message>
                        }
                    </td>
                    <td>Modifies the existing post by id and userKey. Uses userKey to identify rightul owner.</td>
                </tr>
                <tr>
                    <td>POST</td>
                    <td>/thyshallperish</td>
                    <td>userKey</td>
                    <td>-</td>
                    <td>Resets the database. Requires a special userKey to identify as the Admin.</td>
                </tr>
            </table>


## Default users and their keys

```
Frodo     -  7QUY8ujh
Sam       -  6M3BAimf
Pippin    -  rXAB22XP
Merry     -  E9oJ3rf9
Gandalf   -  D52vCZu4
Aragorn   -  KlTCwivO
Legolas   -  ZAlOk3LC
Gimli     -  hdh2gMve
Boromir   -  Oy1wbNqt
Sauron    -  qQLmfgiV
Galadriel -  AJOAZ0qW
Saruman   -  BE5AFqsf
Faramir   -  bP2H3Uxc
Gothmog   -  53IhAIdO
Gollum    -  C9CkOQ6d 
```

Currently running in https://starttipakettiforumapi.azurewebsites.net/
### ! Very Secret !
Current build admin is `mega-admin-imperator-titan`. Please dont exploit :) 
