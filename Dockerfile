FROM node:18-alpine AS builder
WORKDIR /app

COPY ./package*.json ./
COPY ./src ./src
COPY ./tsconfig.json ./tsconfig.json

RUN npm i
RUN npm run build

FROM node:18-alpine AS final
WORKDIR /app

ARG PORT
ARG PG_HOST
ARG PG_PORT
ARG PG_USERNAME
ARG PG_PASSWORD
ARG PG_DATABASE
ARG ADMIN=mega-admin-imperator-titan

ENV PORT=${PORT}
ENV PG_HOST=${PG_HOST}
ENV PG_PORT=${PG_PORT}
ENV PG_USERNAME=${PG_USERNAME}
ENV PG_PASSWORD=${PG_PASSWORD}
ENV PG_DATABASE=${PG_DATABASE}
ENV ADMIN=${ADMIN}

COPY --from=builder ./app/dist ./dist
COPY ./package*.json ./
RUN npm i

EXPOSE 3000
CMD ["npm", "start"]