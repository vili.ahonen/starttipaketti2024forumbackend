"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const messageController_1 = require("./messageController");
const executeQuery_1 = require("./db/executeQuery");
const authMiddleware_1 = require("./middleware/authMiddleware");
const sanitationMiddleware_1 = require("./middleware/sanitationMiddleware");
const nothingFoundMiddleware_1 = require("./middleware/nothingFoundMiddleware");
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use((0, cors_1.default)());
module.exports = app;
const PORT = process.env.PORT;
//InitializeDatabase()
app.get("/allMessages", sanitationMiddleware_1.fetchQueryValidation, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const messages = yield (0, messageController_1.getAllMessages)();
    res.status(200).send(messages);
}));
app.get("/singleMessage/:id", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const message_id = req.params.id;
    const messages = yield (0, messageController_1.getSingleMessage)(Number(message_id));
    res.send(messages);
}));
app.post("/newMessage", authMiddleware_1.UserAuthentication, sanitationMiddleware_1.ValidateNewMessage, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { message } = req.body;
    let key = req.userKey ? req.userKey : "";
    key = Object.values(key)[0];
    const body = { sender_key: key, message: message };
    const result = yield (0, messageController_1.postNewMessage)(body);
    const formattedResponse = `
        <p>Message sent with message_id ${Object.values(result)[0]}.</p>
        <p>You may inspect this message with GET endpoint '/singleMessage/:id'.</p>
    `;
    res.status(201).send(formattedResponse);
}));
app.delete("/deleteMessage/:id", authMiddleware_1.UserAuthentication, sanitationMiddleware_1.ValidateId, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = req.params.id;
    let key = req.userKey ? req.userKey : "";
    key = Object.values(key)[0];
    const target = { sender_key: key, message_id: Number(id) };
    const result = yield (0, messageController_1.deleteMessage)(target);
    if (result) {
        return res.status(200).send(`Deleted message with id: ${Object.values(result)[0]}`);
    }
    res.status(200).send(`Message with id of ${id} was not found or it may not belong to you`);
}));
app.put("/modifyMessage", authMiddleware_1.UserAuthentication, sanitationMiddleware_1.ValidateModifyMessage, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id, message } = req.body;
    let key = req.userKey ? req.userKey : "";
    key = Object.values(key)[0];
    const body = { sender_key: key, message: message, message_id: id };
    const result = yield (0, messageController_1.modifyMessage)(body);
    if (result === undefined) {
        return res.status(401).send(`No message with id ${id} found or it doesn't belong to you`);
    }
    if (Object.values(result)[0] === id) {
        const formattedResponse = `
            <p>Message modified with message_id ${id}.</p>
            <p>You may inspect this message with GET endpoint '/singleMessage/:id'.</p>
        `;
        return res.status(201).send(formattedResponse);
    }
}));
// hidden and loosely protected database reset
app.post("/thyshallperish", authMiddleware_1.AdminAuthentication, (req, res) => {
    (0, executeQuery_1.InitializeDatabase)();
    res.status(200).send("Tom Bombadil did his magic!");
});
app.use(nothingFoundMiddleware_1.NothingFound);
app.listen(PORT, () => {
    console.log(`Listeing to port ${PORT}`);
});
exports.default = app;
