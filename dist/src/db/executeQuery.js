"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InitializeDatabase = exports.executeQuery = void 0;
const pg_1 = __importDefault(require("pg"));
require("dotenv/config");
const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env;
const pool = new pg_1.default.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: String(PG_PASSWORD),
    database: PG_DATABASE,
    ssl: true
});
const executeQuery = (query, params = []) => __awaiter(void 0, void 0, void 0, function* () {
    const client = yield pool.connect();
    try {
        const result = yield client.query(query, params);
        return result;
    }
    catch (error) {
        console.error(error.stack);
        error.name = 'dbError';
        throw error;
    }
    finally {
        client.release();
    }
});
exports.executeQuery = executeQuery;
const InitializeDatabase = () => __awaiter(void 0, void 0, void 0, function* () {
    const creationQuery = `
        DROP TABLE IF EXISTS Users CASCADE;
        DROP TABLE IF EXISTS Messages CASCADE;

        CREATE TABLE IF NOT EXISTS Users (
            user_id serial PRIMARY KEY,
            user_key VARCHAR (50) UNIQUE NOT NULL,
            user_name VARCHAR (50) UNIQUE
        );

        CREATE TABLE IF NOT EXISTS Messages (
            message_id serial PRIMARY KEY,
            sender_key VARCHAR (50) REFERENCES Users (user_key),
            sender_name VARCHAR (50) REFERENCES Users (user_name),
            content VARCHAR (500),
            timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
        );
    `;
    console.log("INITIALIZE DB TABLES");
    try {
        yield (0, exports.executeQuery)(creationQuery);
        console.log("POPULATE TABLES: Users");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Frodo',        '7QUY8ujh');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Sam',          '6M3BAimf');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Pippin',       'rXAB22XP');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Merry',        'E9oJ3rf9');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Gandalf',      'D52vCZu4');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Aragorn',      'KlTCwivO');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Legolas',      'ZAlOk3LC');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Gimli',        'hdh2gMve');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Boromir',      'Oy1wbNqt');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Sauron',       'qQLmfgiV');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Galadriel',    'AJOAZ0qW');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Saruman',      'BE5AFqsf');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Faramir',      'bP2H3Uxc');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Gothmog',      '53IhAIdO');");
        yield (0, exports.executeQuery)("INSERT INTO Users (user_name, user_key) VALUES ('Gollum',       'C9CkOQ6d');");
        // Add admin too
        console.log("POPULATE TABLES: Messages");
        yield (0, exports.executeQuery)("INSERT INTO Messages (sender_name, sender_key, content) VALUES ( (SELECT user_name FROM Users WHERE user_id = 1), (SELECT user_key FROM Users WHERE user_id = 1), 'I''ll take the Ring to the Mordor');");
        yield (0, exports.executeQuery)("INSERT INTO Messages (sender_name, sender_key, content) VALUES ( (SELECT user_name FROM Users WHERE user_id = 9), (SELECT user_key FROM Users WHERE user_id = 9), 'One does not simply walk into Mordor');");
        yield (0, exports.executeQuery)("INSERT INTO Messages (sender_name, sender_key, content) VALUES ( (SELECT user_name FROM Users WHERE user_id = 2), (SELECT user_key FROM Users WHERE user_id = 2), 'Po-ta-tos!');");
        yield (0, exports.executeQuery)("INSERT INTO Messages (sender_name, sender_key, content) VALUES ( (SELECT user_name FROM Users WHERE user_id = 7), (SELECT user_key FROM Users WHERE user_id = 7), 'They are taking the hobbits to Isengard!');");
        console.log("INITIALIZATION COMPLETED");
    }
    catch (error) {
        console.log("DB INIT ERROR\n", error);
    }
});
exports.InitializeDatabase = InitializeDatabase;
