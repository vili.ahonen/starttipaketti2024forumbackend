"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NothingFound = void 0;
const NothingFound = (req, res) => {
    const guide = `
        <html>
        <head>
            <style>
                table{
                    border-collapse: collapse;
                }
                tr {
                    border: 1px solid black;
                }
                td {
                    padding: 20 10 20 10;
                    border: 1px solid black;
                }
            </style>
        </head>
        <body>
            <h2> Welcome to Forum API </h3>
            <p>v0.1</p>
            <p>Here's a short guide for this API</p>
            <br>
            <table >
                <tr>
                    <th>Method</th>
                    <th>Path</th>
                    <th>Headers</th>
                    <th>Body</th>
                    <th>Description</th>
                </tr>
                <tr>
                    <td>GET</td>
                    <td>/allMessages</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Returns all messages.</td>
                </tr>
                <tr>
                    <td>GET</td>
                    <td>/singleMessage/:id</td>
                    <td>-</td>
                    <td>-</td>
                    <td>Returns single message by id.</td>
                </tr>
                <tr>
                    <td>POST</td>
                    <td>/newMessage</td>
                    <td>userKey</td>
                    <td>
                        {
                        "message": < string >
                        }
                    </td>
                    <td>Posts a new message that can be read.</td>
                </tr>
                <tr>
                    <td>DELETE</td>
                    <td>/deleteMessage/:id</td>
                    <td>userKey</td>
                    <td>-</td>
                    <td>Deletes a post by this user. Uses userKey to identify rightful owner.</td>
                </tr>
                <tr>
                    <td>PUT</td>
                    <td>/modifyMessage</td>
                    <td>userKey</td>
                    <td>
                        {
                        "id": < number > ,
                        "message": < string > 
                        }
                    </td>
                    <td>Modifies the existing post by id and userKey. Uses userKey to identify rightul owner.</td>
                </tr>
            </table>
        </body>
        </html>
    `;
    res.status(404).send(guide);
};
exports.NothingFound = NothingFound;
