"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.modifyMessage = exports.deleteMessage = exports.postNewMessage = exports.getSingleMessage = exports.getAllMessages = exports.validateUser = void 0;
const executeQuery_1 = require("./db/executeQuery");
const validateUser = (userkey) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT user_key FROM Users WHERE user_key = $1`;
        const result = yield (0, executeQuery_1.executeQuery)(query, [userkey]);
        if (result.rows.length === 1) {
            return result.rows[0];
        }
        return false;
    }
    catch (e) {
        return e;
    }
});
exports.validateUser = validateUser;
const getAllMessages = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT message_id, sender_key, sender_name, content, timestamp FROM Messages ORDER BY timestamp ASC`;
        const result = yield (0, executeQuery_1.executeQuery)(query);
        return result.rows;
    }
    catch (e) {
        return e;
    }
});
exports.getAllMessages = getAllMessages;
const getSingleMessage = (message_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT message_id, sender_name, content, timestamp FROM Messages WHERE message_id = $1`;
        const result = yield (0, executeQuery_1.executeQuery)(query, [String(message_id)]);
        return result.rows;
    }
    catch (e) {
        return e;
    }
});
exports.getSingleMessage = getSingleMessage;
const postNewMessage = (message) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const params = [message.sender_key, message.message];
        console.log("params: ", params);
        const query = `
            INSERT INTO Messages 
                (sender_name, sender_key, content) 
            VALUES 
                ( 
                    (SELECT user_name FROM Users
                        WHERE user_key = $1),
                    $1, $2
                )
                RETURNING message_id, content`;
        const result = yield (0, executeQuery_1.executeQuery)(query, params);
        return result.rows[0];
    }
    catch (e) {
        console.log("Error: ", e);
        return e;
    }
});
exports.postNewMessage = postNewMessage;
const deleteMessage = (target) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `DELETE FROM Messages WHERE sender_key = $1 and message_id = $2 RETURNING message_id`;
        const result = yield (0, executeQuery_1.executeQuery)(query, [target.sender_key, String(target.message_id)]);
        return result.rows[0];
    }
    catch (e) {
        return e;
    }
});
exports.deleteMessage = deleteMessage;
const modifyMessage = (message) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `
            UPDATE Messages SET
                content = $2
            WHERE sender_key = $1
            AND message_id = $3
            RETURNING message_id, content
        `;
        const result = yield (0, executeQuery_1.executeQuery)(query, [message.sender_key, String(message.message), String(message.message_id)]);
        console.log("Result: ", result.rows[0]);
        return result.rows[0];
    }
    catch (e) {
        return e;
    }
});
exports.modifyMessage = modifyMessage;
