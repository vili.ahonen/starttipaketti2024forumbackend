"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminAuthentication = exports.UserAuthentication = void 0;
const messageController_1 = require("../messageController");
const UserAuthentication = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const userKey = req.get("userkey");
    if (!userKey) {
        return res.status(401).send("Missing 'userkey' header.");
    }
    else {
        const validatedUserKey = yield (0, messageController_1.validateUser)(String(userKey));
        console.log("Validation. ", validatedUserKey);
        if (validatedUserKey) {
            req.userKey = validatedUserKey;
            console.log("req: ", req.userKey);
            next();
        }
        else {
            return res.status(401).send("Invalid 'userkey' header.");
        }
    }
});
exports.UserAuthentication = UserAuthentication;
const AdminAuthentication = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const userKey = req.get("userkey");
    if (!userKey) {
        return res.status(401).send("Missing 'userkey' header.");
    }
    else {
        const adminKey = process.env.ADMIN;
        console.log("adminKey. ", adminKey);
        if (adminKey === userKey) {
            next();
        }
        else {
            return res.status(401).send("Invalid 'userkey' header.");
        }
    }
});
exports.AdminAuthentication = AdminAuthentication;
