"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidateId = exports.fetchQueryValidation = exports.ValidateModifyMessage = exports.ValidateNewMessage = void 0;
const ValidateNewMessage = (req, res, next) => {
    var _a, _b;
    const body = req.body;
    console.log("MEssage: ", body.message);
    if (!body.message) {
        return res.status(400).send("Missing 'message' property within body or it's empty");
    }
    if (((_a = body.message) === null || _a === void 0 ? void 0 : _a.length) > 500) {
        return res.status(400).send(`Message is too long. Maximum character count is 500 and you had ${(_b = body.message) === null || _b === void 0 ? void 0 : _b.length}`);
    }
    next();
};
exports.ValidateNewMessage = ValidateNewMessage;
const ValidateModifyMessage = (req, res, next) => {
    var _a, _b;
    const body = req.body;
    if (!body.id) {
        return res.status(400).send("Missing 'id' property within body");
    }
    console.log(typeof (body.id));
    if (typeof (body.id) !== "number") {
        return res.status(400).send("Invalid 'id' property, it should be number");
    }
    if (!body.message) {
        return res.status(400).send("Missing 'message' property within body or it's empty");
    }
    if (((_a = body.message) === null || _a === void 0 ? void 0 : _a.length) > 500) {
        return res.status(400).send(`Message is too long. Maximum character count is 500 and you had ${(_b = body.message) === null || _b === void 0 ? void 0 : _b.length}`);
    }
    next();
};
exports.ValidateModifyMessage = ValidateModifyMessage;
const fetchQueryValidation = (req, res, next) => {
    const order = req.query.order ? String(req.query.order).toLowerCase() : "desc";
    const orders = ["asc", "desc"];
    if (order && !orders.includes(order)) {
        return res.status(400).send("Query parameter 'order' must be either 'asc' or 'desc'.");
    }
    next();
};
exports.fetchQueryValidation = fetchQueryValidation;
const ValidateId = (req, res, next) => {
    const id = req.params.id;
    if (!Number(id)) {
        return res.status(400).send("Invalid id. Id must be a number");
    }
    next();
};
exports.ValidateId = ValidateId;
